## Sources:
---

- [GitLab Documentation](https://about.gitlab.com/blog/2022/03/15/publishing-obsidian-notes-with-gitlab-pages/)
- [GitLab Demo](https://gitlab.com/gitlab-org/frontend/playground/obsidian-and-gitlab-pages-demo)
- [Perso Demo](https://gitlab.com/cecilevermesse/obsidian/-/tree/master)
- [Obsidian.md](https://obsidian.md)
- [MkDocs](https://www.mkdocs.org/)

## MkDocs
& On utilise MkDocs pour transformer le dossier Obsidian en site statique. Il existe de nombreuses extensions.

## Codes :

3 fichiers de configurations sont nécessaires. Le Vault d'Obsidian doit être dans un sous dossier du projet (par défaut : `docs`).

- `.gitlab-ci.yml`
```
image: python:3.8-slim  
  
pages:  
  stage: deploy  
  script:  
    - pip install -r requirements.txt  
    - mkdocs build --strict --verbose  
  artifacts:  
    paths:  
      - public  
  only:  
    - master
```
- `requirements.txt`
```
# Documentation static site generator & deployment tool  
mkdocs>=1.1.2  
  
# Material theme  
mkdocs-material>=8.1.7  
  
# Wikilinks support  
mkdocs-roamlinks-plugin>=0.1.3
```
- `mkdocs.yml`
```
site_name: Notes Techniques  
site_url: https://cecilevermesse.gitlab.io/obsidian  
site_dir: public  
  
theme:  
  name: material  
  palette:  
    scheme: slate  
  
# Extensions  
markdown_extensions:  
  - footnotes  
  - attr_list  
  - admonition  
  - pymdownx.highlight  
  - pymdownx.superfences  
  - pymdownx.details  
  - pymdownx.magiclink  
  - pymdownx.tasklist  
  - pymdownx.emoji  
  - toc:  
      permalink: true  
  
plugins:  
  - search  
  - roamlinks
```


On peut controller l'accès aux pages GitLab dans **Settings > General** puis dans **Visibility, project features, permissions** 

## Automatiser les commits

Il existe un plugin d'auto-commit sur Obsidian. Voir les limitations concernant le travail collaboratif.


## Limitation 

On ne peut pas afficher le graph de dépendance que génère Obsidian.
Nécessite l'installation et la configuration de Git sur son PC.
Problématique sur Mobile.
Restriction des droits à la GitLab Page ne peut se faire qu'avec les comptes GitLab